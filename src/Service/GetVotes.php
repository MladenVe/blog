<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;


class GetVotes {
  private $entityManager;

  public function __construct(EntityManagerInterface $entityManager){
    $this->entityManager = $entityManager;
  }

  public function getScore() {
    $args = [];
    $args['side1'] = $this->entityManager->getRepository('App:Vote')->countVotes(1);
    $args['side2'] = $this->entityManager->getRepository('App:Vote')->countVotes(2);

    $fp = fopen('results.json', 'w');
    fwrite($fp, json_encode($args));
    fclose($fp);
  }
}