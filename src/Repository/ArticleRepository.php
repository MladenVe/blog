<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    function save($data, $Article) {
        $em = $this->getEntityManager();

        $Article->setTitle($data->getTitle());
        $Article->setAuthor($data->getAuthor());
        $Article->setContent($data->getContent());
        $Article->setCategory($data->getCategory());

        if (empty($Article->getId())) {
            $em->persist($Article);
        }
    }

    public function getAll($connection) {
        $articles = [];
        $sql = "SELECT id, title, author, content, category, created, updated 
                FROM article";
        $result = $connection->prepare($sql);
        $result->execute();
        while ($rs = $result->fetch()) {
          $articles[] = $rs;
        }
        return $articles;
      }

    function saveOverApi($data) {
        $em = $this->getEntityManager();

        $Article = new Article();
        $Article->setTitle($data['title']);
        $Article->setAuthor($data['author']);
        $Article->setContent($data['category']);
        $Article->setCategory($data['content']);
        $em->persist($Article);
        $em->flush();
    }
    

}
