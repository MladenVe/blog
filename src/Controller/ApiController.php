<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Doctrine\DBAL\Driver\Connection;

class ApiController extends AbstractController
{

    /**
     * @Route("/save-article", options={"expose"=true})
     */
    public function saveArticleAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        header('Access-Control-Allow-Origin: *');

        $data['title'] = $request->request->get('title');
        $data['author'] = $request->request->get('author');
        $data['category'] = $request->request->get('category');
        $data['content'] = $request->request->get('content');
        
        if (isset($data['title']) && isset($data['author']) && isset($data['category']) && isset($data['content'])) {
            $em->getRepository('App:Article')->saveOverApi($data, null);
        }

        return new Response('');
    }

    /**
     * @Route("/get-article", name="test", options={"expose"=true})
     */
    public function getArticleAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        header('Access-Control-Allow-Origin: *');

        if ($request->query->get('id') !== null) {
            $article = $em->getRepository('App:Article')->findOneBy(['id' => $request->query->get('id')]);
        } elseif ($request->query->get('title') !== null) {
            $article = $em->getRepository('App:Article')->findOneBy(['title' => $request->query->get('title')]);
        } elseif ($request->query->get('author') !== null) {
            $article = $em->getRepository('App:Article')->findOneBy(['author' => $request->query->get('author')]);
        } elseif ($request->query->get('category') !== null) {
            $article = $em->getRepository('App:Article')->findOneBy(['category' => $request->query->get('category')]);
        } else {
            $article = 'There is not one article by the required criteria.';
        }

        return new JsonResponse($article);
    }

    /**
     * @Route("/list-article", name="test", options={"expose"=true})
     */
    public function listArticleAction(Connection $connection) {
        $em = $this->getDoctrine()->getManager();
        header('Access-Control-Allow-Origin: *');

        $args['articles'] = $em->getRepository('App:Article')->getAll($connection);

        return new JsonResponse($args);
    }
}
