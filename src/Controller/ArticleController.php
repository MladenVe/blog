<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use App\Entity\Article;
use App\Form\ArticleFormType;

class ArticleController extends AbstractController
{
    /**
     * @Route("/save/{id}", defaults={"id" = null})
     */
    public function articleAction(Request $request, Article $Article = null) {
        $em = $this->getDoctrine()->getManager();
        $args = [];

        if (!empty($Article)) {
            $args['article'] = $Article;
            $id = $Article->getId();
        } else {
            $Article = new Article();
        }

        $form = $this->createForm(ArticleFormType::class, $Article, [ 'action' => $this->generateUrl('app_article_article', isset($id) ? ['id' => $id] : []), 'method' => 'POST' ]);
        $form->handleRequest($request);

        $args['form'] = $form->createView();
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $data = $form->getData();
                $em->getRepository('App:Article')->save($data, $Article);
                $em->flush();

            } catch (\Exception $e) {
                error_log(json_encode(array(__FILE__, __LINE__, $e->getMessage())));
                $this->addFlash('notice', 'An error has occurred');
                return $this->redirectToRoute('app_article_article');
            }
            return $this->redirectToRoute('home');
        }

        return $this->render('article/save.html.twig', $args);
    }

    /**
     * @Route("/article/{id}")
     */
    public function showAction(Article $Article) {
        $args = [];
        $args['article'] = $Article;

        return $this->render('article/show.html.twig', $args);
    }

    /**
     * @Route("/delete/{id}")
     */
    public function deleteAction(Article $Article) {
        $em = $this->getDoctrine()->getManager();

        $em->remove($Article);
        $em->flush();

        return $this->redirectToRoute('home');
    }

}
