<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

use Doctrine\DBAL\Driver\Connection;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class StorageController extends AbstractController
{

    /**
     * @Route("/saveblog-json", options={"expose"=true})
     */
    public function saveblogjsonAction(Connection $connection, Request $request) {
        $em = $this->getDoctrine()->getManager();

        header('Access-Control-Allow-Origin: *');

        $args = [];
        $args['articles'] = $em->getRepository('App:Article')->getAll($connection);
        foreach ($args['articles'] as $article) {
            $args[] = $article;
        }

        $fp = fopen('articles.json', 'w');
        fwrite($fp, json_encode($args));
        fclose($fp);

		return new Response('');
    }

    /**
     * @Route("/saveblog-xls", options={"expose"=true})
     */
    function exportfilleddataAction(Connection $connection, Request $request) {
        set_time_limit(0);
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('App:Article')->getAll($connection);

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('Htec')
                    ->setTitle('Blog');
        $titles = [ 'id', 'title', 'author', 'content', 'category', 'created' ];
        $fields = [ 'A', 'B', 'C', 'D', 'E', 'F' ];

        $spreadsheet->setActiveSheetIndex(0);
        for ($i = 0; $i < count($titles); $i++) {
            $spreadsheet->getActiveSheet()->setCellValue($fields[$i] . '1', $titles[$i]);
        }
        $i = 2;
        foreach ($articles as $key => $value) {
            $spreadsheet->getActiveSheet()
                        ->setCellValue($fields[0] . $i, $value['id'])
                        ->setCellValue($fields[1] . $i, $value['title'])
                        ->setCellValue($fields[2] . $i, $value['author'])
                        ->setCellValue($fields[3] . $i, $value['content'])
                        ->setCellValue($fields[4] . $i, $value['category'])
                        ->setCellValue($fields[5] . $i, $value['created']);
            $i++;
        }
        foreach ($fields as $key => $value) {
            $spreadsheet->getActiveSheet()->getColumnDimension($value)->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->setTitle('Blog')
                    ->getPageSetup()
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4)
                    ->setFitToPage(true);
        $writer = new Xls($spreadsheet);

        $response = new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        // $winnerDate
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'Blog_' . (new \DateTime())->format('Ymd') . '.xls' 
        );
        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }
    
}
